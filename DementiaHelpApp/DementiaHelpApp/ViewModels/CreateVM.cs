﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class CreateVM
    {
        [Required(ErrorMessage = "Proszę podaj nazwę użytkownika.")]
        [Display(Name = "Nazwa użytkownika")]
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression("[0-9]{9}", ErrorMessage = "Numer telefonu musi składać się z 9 cyfr.")]
        [Required(ErrorMessage = "Proszę podaj numer telefonu.")]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Proszę podaj hasło.")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Proszę powtórz hasło.")]
        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        public string ConfirmPassword { get; set; }

        //[Required(ErrorMessage = "Proszę wybierz.")]
        [EnumDataType(typeof(Role))]
        [Display(Name = "Rola")]
        public Role Roles { get; set; }
    }
    public enum Role
    {
        [Display(Name = "Pacjent")]
        Patient,
        [Display(Name = "Opiekun")]
        Caregiver
    }
}

