﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class CreateCaregiverVM: CreateVM
    {
        [Required(ErrorMessage = "Proszę podaj imię.")]
        [Display(Name = "Imię")]
        //[RegularExpression("/^[A-ZŁŚŻŹĆ]{1}[a-ząęółśżźćń]*/g", ErrorMessage = "Niepoprawne imię.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Proszę podaj nazwisko.")]
        [Display(Name = "Nazwisko")]
        //[RegularExpression("/^([A-ZŁŚŻŹĆ]{1}[a-ząęółśżźćń]{2,})+([-]{0,1}[A-ZŁŚŻŹĆ]{1}[a-ząęółśżźćń]*)|^([A-ZŁŚŻŹĆ]{1}[a-ząęółśżźćń]*)/g", ErrorMessage = "Niepoprawne nazwisko.")]
        public string LastName { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        public void SetRole()
        {
            Roles = Role.Caregiver;
        }

    }
}
