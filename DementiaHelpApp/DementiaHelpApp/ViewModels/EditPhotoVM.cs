﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class EditPhotoVM
    {
        [Display(Name = "Plik")]
        public IFormFile FormFile { get; set; }

        [Required(ErrorMessage = "Proszę podaj opis zdjęcia.")]
        [Display(Name = "Opis")]
        public string Description { get; set; }
    }
}
