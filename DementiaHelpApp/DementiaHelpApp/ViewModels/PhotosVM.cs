﻿using DementiaHelpApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class PhotosVM
    {
        public string Title { get; set; }
        public List<PatientPhoto> PatientPhotos { get; set; }
        public List<CaregiverPhoto> CaregiverPhotos { get; set; }
    }
}
