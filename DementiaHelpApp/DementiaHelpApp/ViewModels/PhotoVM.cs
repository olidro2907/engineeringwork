﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class PhotoVM
    {
        [Required(ErrorMessage = " ")]
        [Display(Name = "Plik")]
        public IFormFile FormFile { get; set; }

        [Required(ErrorMessage = "Proszę wpisz komentarz do zdjęcia.")]
        [Display(Name = "Opis")]
        public string Description { get; set; }
    }
}
