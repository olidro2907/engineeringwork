﻿using DementiaHelpApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class ManyPatientPhotosVM
    {
        public string Title { get; set; }
        public List<string> PatientName { get; set; }
        public List<List<PatientPhoto>> PatientPhotos { get; set; }
        public List<CaregiverPhoto> CaregiverPhotos { get; set; }
    }
}
