﻿using DementiaHelpApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class PatientPhotosVM
    {
        public string Title { get; set; }
        public List<PatientPhoto> Photos { get; set; }
        public EditPhotoVM PhotoVM { get; set; }
    }
}
