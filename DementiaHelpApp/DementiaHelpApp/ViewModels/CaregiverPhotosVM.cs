﻿using DementiaHelpApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class CaregiverPhotosVM
    {
        public string Title { get; set; }
        public List<CaregiverPhoto> Photos { get; set; }
        public EditPhotoVM PhotoVM { get; set; }
    }
}
