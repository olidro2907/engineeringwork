﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class LoginVM
    {
        [Required(ErrorMessage = "Proszę podaj nazwę użytkownika.")]
        [Display(Name ="Nazwa użytkownika")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Proszę podaj hasło.")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Zapamiętaj mnie")]
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}
