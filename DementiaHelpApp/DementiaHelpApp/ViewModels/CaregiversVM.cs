﻿using DementiaHelpApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class CaregiversVM
    {
        public string Title { get; set; }
        public List<Caregiver> Caregivers { get; set; }
    }
}
