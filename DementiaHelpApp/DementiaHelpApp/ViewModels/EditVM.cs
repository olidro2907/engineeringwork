﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class EditVM
    {
        [Display(Name = "Nazwa użytkownika")]
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Numer telefonu"), DataType(DataType.PhoneNumber)]
        [RegularExpression("[0-9]{9}", ErrorMessage = "Numer telefonu musi składać się z 9 cyfr.")]
        [Phone]
        public string PhoneNumber { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nowe hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Stare hasło")]
        public string OldPassword { get; set; }

        public string UserId { get; set; }

        public string ReturnUrl { get; set; }

        //[Required(ErrorMessage = "Proszę wybierz.")]
        [EnumDataType(typeof(Role))]
        [Display(Name = "Rola")]
        public Role Roles { get; set; }
    }
}

