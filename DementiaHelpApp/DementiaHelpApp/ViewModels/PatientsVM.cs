﻿using DementiaHelpApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.ViewModels
{
    public class PatientsVM
    {
        public string Title { get; set; }
        public List<Patient> Patients { get; set; }
    }
}
