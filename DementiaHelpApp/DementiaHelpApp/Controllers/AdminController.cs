﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DementiaHelpApp.Data;
using DementiaHelpApp.Models;
using DementiaHelpApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace DementiaHelpApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly AppDbContext _db;
        private IUserValidator<User> _userValidator;
        private IPasswordValidator<User> _passwordValidator;
        private IPasswordHasher<User> _passwordHasher;
        public AdminController(UserManager<User> userManager, AppDbContext db, IUserValidator<User> userValidator, IPasswordValidator<User> passwordValidator, IPasswordHasher<User> passwordHasher)
        {
            _userManager = userManager;
            _db = db;
            _userValidator = userValidator;
            _passwordValidator = passwordValidator;
            _passwordHasher = passwordHasher;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreateCaregiver()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreatePatient()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Update(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                EditVM userVM = new EditVM { UserName = user.UserName, Email = user.Email, UserId = user.Id };
                return View(userVM);
            }
            else
            {               
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public async Task<IActionResult> UpdateCaregiver(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var caregiver = _db.Caregivers.FirstOrDefault(c => c.UserId == user.Id);
                if(caregiver == null)
                {
                    return NotFound();
                }

                EditCaregiverVM caregiverVM = new EditCaregiverVM { UserName=user.UserName, Email = caregiver.Email, PhoneNumber = caregiver.PhoneNumber, FirstName = caregiver.FirstName, LastName = caregiver.LastName, Description=caregiver.Description, UserId=user.Id};

                return View(caregiverVM);
            }
            else
            {
                return RedirectToAction("Caregivers");
            }
        }

        [HttpGet]
        public async Task<IActionResult> UpdatePatient(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);
                if (patient == null)
                {
                    return NotFound();
                }
                EditPatientVM patientVM = new EditPatientVM { UserName=user.UserName, Email = patient.Email, FirstName = patient.FirstName, LastName = patient.LastName, PhoneNumber=patient.PhoneNumber, Date_of_birth = patient.Date_of_birth, RoomNumber = patient.RoomNumber, Description = patient.Description, UserId = user.Id };

                return View(patientVM);
            }
            else
            {
                return RedirectToAction("Patients");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCaregiver(CreateCaregiverVM caregiverVM)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(caregiverVM.UserName);
                if (user == null)
                {
                    user = new User()
                    {
                        UserName = caregiverVM.UserName,
                        Email = caregiverVM.Email,
                        PhoneNumber = caregiverVM.PhoneNumber,
                        AddDate = DateTime.Now
                    };
                    if (caregiverVM.Password == caregiverVM.ConfirmPassword)
                    {
                        var result = await _userManager.CreateAsync(user, caregiverVM.Password);

                        if (result.Succeeded)
                        {
                            caregiverVM.SetRole();
                            await _userManager.AddToRoleAsync(user, caregiverVM.Roles.ToString());
                            await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, caregiverVM.Roles.ToString()));

                            if (caregiverVM.Roles.ToString().Contains("Caregiver"))
                            {
                                Caregiver caregiver = new Caregiver { AddDate = user.AddDate, Email = user.Email, PhoneNumber= user.PhoneNumber, FirstName = caregiverVM.FirstName.Trim(), LastName = caregiverVM.LastName.Trim(), Description=caregiverVM.Description, UserId = user.Id, User = user };
                                _db.Caregivers.Add(caregiver);
                                _db.SaveChanges();

                                if (_db.Patients.Any())
                                {
                                    Caregiver copycaregiver = _db.Caregivers.FirstOrDefault(a => a.Id == caregiver.Id);
                                    foreach (var patient in _db.Patients)
                                    {
                                        _db.CaregiverPatients.Add(new CaregiverPatient { Caregiver = copycaregiver, Patient = patient });
                                    }
                                    _db.SaveChanges();
                                }
                            }
                            return RedirectToAction("Caregivers");
                        }
                        else
                        {
                            var list_of_errors = result.Errors.ToList();
                            foreach (var error in list_of_errors)
                            {
                                if (error.Code.Contains("Password"))
                                {
                                    ModelState.AddModelError("password", error.Description);
                                }
                                else if (error.Code.Contains("User"))
                                {
                                    ModelState.AddModelError("username", error.Description);
                                }
                                else if (error.Code.Contains("Email"))
                                {
                                    ModelState.AddModelError("email", error.Description);
                                }
                            }
                        }
                    }
                    else
                        ModelState.AddModelError("password", "Podane hasło i powtórzone hasło musi się zgadzać.");
                }
                else
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
            }
            return View(caregiverVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatePatient(CreatePatientVM patientVM)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(patientVM.UserName);
                if (user == null)
                {
                    user = new User()
                    {
                        UserName = patientVM.UserName,
                        Email = patientVM.Email,
                        AddDate = DateTime.Now
                    };
                    if (patientVM.Password == patientVM.ConfirmPassword)
                    {
                        var min_data = new DateTime(1900, 1, 1);
                        var default_data = new DateTime(0001, 01, 01);

                        if (patientVM.Date_of_birth < DateTime.Now && patientVM.Date_of_birth >= min_data)
                        {
                            var result = await _userManager.CreateAsync(user, patientVM.Password);

                            if (result.Succeeded)
                            {
                                patientVM.SetRole();
                                await _userManager.AddToRoleAsync(user, patientVM.Roles.ToString());
                                await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, patientVM.Roles.ToString()));

                                if (patientVM.Roles.ToString().Contains("Patient"))
                                {

                                    if (_db.Caregivers.Any())
                                    {

                                        Patient patient = new Patient { AddDate = user.AddDate, Email = user.Email, FirstName = patientVM.FirstName.Trim(), LastName = patientVM.LastName.Trim(), Date_of_birth = patientVM.Date_of_birth.GetValueOrDefault(), RoomNumber = patientVM.RoomNumber.GetValueOrDefault(), Description = patientVM.Description, Confirm = true, UserId = user.Id, User = user };
                                        _db.Patients.Add(patient);
                                        _db.SaveChanges();

                                        if (_db.Caregivers.Any())
                                        {
                                            Patient copypatient = _db.Patients.FirstOrDefault(a => a.Id == patient.Id);
                                            foreach (var caregiver in _db.Caregivers)
                                            {
                                                _db.CaregiverPatients.Add(new CaregiverPatient { Caregiver = caregiver, Patient = copypatient });
                                            }
                                            _db.SaveChanges();
                                        }

                                        return RedirectToAction("Patients");
                                    }
                                    else
                                    {
                                        IdentityResult unresult = await _userManager.DeleteAsync(user);
                                        return RedirectToAction("Patients");
                                    }


                                }

                            }
                            else
                            {
                                var list_of_errors = result.Errors.ToList();
                                foreach (var error in list_of_errors)
                                {
                                    if (error.Code.Contains("Password"))
                                    {
                                        ModelState.AddModelError("password", error.Description);
                                    }
                                    else if (error.Code.Contains("User"))
                                    {
                                        ModelState.AddModelError("username", error.Description);
                                    }
                                    else if (error.Code.Contains("Email"))
                                    {
                                        ModelState.AddModelError("email", error.Description);
                                    }
                                }
                            }
                        }
                        else
                            ModelState.AddModelError("date_of_birth", "Niepoprawna data urodzenia.");
                    }
                    else
                        ModelState.AddModelError("password", "Podane hasło i powtórzone hasło musi się zgadzać.");
                }
                else
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
            }
            return View(patientVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(EditVM userVM)
        {
            if(ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(userVM.UserId);
                bool change_userName = false;
                bool change_phoneNumber = false;
                bool change_email = false;
                bool change_password = false;

                if (user != null)
                {
                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(userVM.UserName) && userVM.UserName != user.UserName)
                    {
                        user.UserName = userVM.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(userVM);
                        }
                        change_userName = true;
                    }

                    if (!string.IsNullOrEmpty(userVM.PhoneNumber) && userVM.PhoneNumber != user.PhoneNumber)
                    {
                        if(userVM.PhoneNumber.Length==9)
                        {
                            user.PhoneNumber = userVM.PhoneNumber;
                            change_phoneNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("phonenumber", "Numer telefonu musi się składać z 9 cyfr.");
                            return View(userVM);
                        }
                        
                    }

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(userVM.Email) && userVM.Email != user.Email)
                    {
                        user.Email = userVM.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {                        
                            AddErrorsFromResult(validEmail);
                            return View(userVM);
                        }
                        change_email = true;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(userVM.Password)) && (!string.IsNullOrEmpty(userVM.ConfirmPassword)) && (!string.IsNullOrEmpty(userVM.OldPassword)) && (userVM.OldPassword != userVM.Password) && (userVM.Password == userVM.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, userVM.Password);
                        if (validPass.Succeeded)
                        {
                            var result = _userManager.ChangePasswordAsync(user, userVM.OldPassword, userVM.Password);
                            if(result.Result.Succeeded)
                            {
                                user.PasswordHash = _passwordHasher.HashPassword(user, userVM.Password);
                                change_password = true;
                            }
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                            return View(userVM);
                        }
                    }
                    else if ((!string.IsNullOrEmpty(userVM.OldPassword)) && (!string.IsNullOrEmpty(userVM.Password)) && (userVM.OldPassword == userVM.Password))
                    {
                        ModelState.AddModelError("oldpassword", "Stare hasło i nowe hasło musi być różne.");
                        return View(userVM);
                    }
                    else if (userVM.Password != userVM.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(userVM);
                    }


                    if (change_userName || change_phoneNumber || change_email || change_password)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nie znaleziono użytkownika o takiej nazwie.");
                }
            }
            
            return View(userVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateCaregiver(EditCaregiverVM caregiverVM)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(caregiverVM.UserId);
                bool change_userName = false;
                bool change_firstName = false;
                bool change_lastName = false;
                bool change_description = false;
                bool change_phoneNumber = false;
                bool change_email = false;
                bool change_password = false;

                if (user != null)
                {
                    var caregiver = _db.Caregivers.FirstOrDefault(p => p.UserId == user.Id);
                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);

                    if (!string.IsNullOrEmpty(caregiverVM.UserName) && caregiverVM.UserName != user.UserName)
                    {
                        var old_userName = user.UserName;
                        user.UserName = caregiverVM.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(caregiverVM);
                        }

                        var old_path_user = Path.Combine(
                                Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", old_userName);

                        var path_username = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName);
                        if (!System.IO.File.Exists(path_username))
                        {
                            Directory.CreateDirectory(path_username);
                        }
                        if (System.IO.Directory.Exists(old_path_user))
                        {
                            string[] dates = Directory.GetFileSystemEntries(old_path_user);
                            foreach (var item in dates)
                            {
                                var date = Path.GetFileName(item);
                                var path_date = Path.Combine(
                                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName, date);
                                if (!System.IO.File.Exists(path_date))
                                {
                                    Directory.CreateDirectory(path_date);
                                }
                                string[] files = Directory.GetFiles(item);
                                foreach (var file in files)
                                {
                                    var filename = Path.GetFileName(file);
                                    var path = Path.Combine(
                                        Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName, date, filename);
                                    string dbpath = user.UserName + "/" + date + "/" + filename;

                                    if (System.IO.File.Exists(file))
                                    {
                                        System.IO.File.Move(file, path);
                                        var photo = _db.CaregiverPhotos.FirstOrDefault(c => c.FileName == filename);
                                        photo.FilePath = dbpath;
                                        _db.Update(photo);
                                        await _db.SaveChangesAsync();

                                    }
                                }
                            }
                        }
                        change_userName = true;
                    }

                    if (!string.IsNullOrEmpty(caregiverVM.FirstName.Trim()) && caregiverVM.FirstName.Trim() != caregiver.FirstName.Trim())
                    {
                        caregiver.FirstName = caregiverVM.FirstName.Trim();
                        change_firstName = true;
                    }

                    if (!string.IsNullOrEmpty(caregiverVM.LastName.Trim()) && caregiverVM.LastName.Trim() != caregiver.LastName.Trim())
                    {
                        caregiver.LastName = caregiverVM.LastName.Trim();
                        change_lastName = true;
                    }

                    if (!string.IsNullOrEmpty(caregiverVM.Description) && caregiverVM.Description != caregiver.Description)
                    {
                        caregiver.Description = caregiverVM.Description;
                        change_description = true;
                    }

                    if (!string.IsNullOrEmpty(caregiverVM.PhoneNumber) && caregiverVM.PhoneNumber != user.PhoneNumber)
                    {
                        if (caregiverVM.PhoneNumber.Length == 9)
                        {
                            user.PhoneNumber = caregiverVM.PhoneNumber;
                            caregiver.PhoneNumber = caregiverVM.PhoneNumber;
                            change_phoneNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("phonenumber", "Numer telefonu musi się składać z 9 cyfr.");
                            return View(caregiverVM);
                        }

                    }

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(caregiverVM.Email) && caregiverVM.Email != user.Email)
                    {
                        user.Email = caregiverVM.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {
                            AddErrorsFromResult(validEmail);
                            return View(caregiverVM);
                        }
                        change_email = true;
                        caregiver.Email = caregiverVM.Email;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(caregiverVM.Password)) && (caregiverVM.Password == caregiverVM.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, caregiverVM.Password);
                        if (validPass.Succeeded)
                        {
                            user.PasswordHash = _passwordHasher.HashPassword(user, caregiverVM.Password);
                            change_password = true;
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                            return View(caregiverVM);
                        }
                    }
                    else if (caregiverVM.Password != caregiverVM.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(caregiverVM);
                    }

                    if (change_userName || change_phoneNumber || change_email || change_password || change_firstName || change_lastName || change_description)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            caregiver.UpdateDate = DateTime.Now;
                            _db.Update(caregiver);
                            _db.SaveChanges();
                            return RedirectToAction("Caregivers");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                            return View(caregiverVM);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
                }
            }
            
            return View(caregiverVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdatePatient(EditPatientVM patientVM)
        {
           if(ModelState.IsValid)
           { 
                User user = await _userManager.FindByIdAsync(patientVM.UserId);
                bool change_userName = false;
                bool change_firstName = false;
                bool change_lastName = false;
                bool change_description = false;
                bool change_phoneNumber = false;
                bool change_email = false;
                bool change_password = false;
                bool change_roomNumber = false;
                bool change_dateOfBirth = false;

                if (user != null)
                {
                    var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);

                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);

                    if (!string.IsNullOrEmpty(patientVM.UserName) && patientVM.UserName != user.UserName)
                    {
                        var old_userName = user.UserName;
                        user.UserName = patientVM.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(patientVM);
                        }

                        var old_path_user = Path.Combine(
                                Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", old_userName);

                        var path_username = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName);
                        if (!System.IO.File.Exists(path_username))
                        {
                            Directory.CreateDirectory(path_username);
                        }
                        if (System.IO.Directory.Exists(old_path_user))
                        {
                            string[] dates = Directory.GetFileSystemEntries(old_path_user);
                            foreach (var item in dates)
                            {
                                var date = Path.GetFileName(item);
                                var path_date = Path.Combine(
                                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, date);
                                if (!System.IO.File.Exists(path_date))
                                {
                                    Directory.CreateDirectory(path_date);
                                }
                                string[] files = Directory.GetFiles(item);
                                foreach (var file in files)
                                {
                                    var filename = Path.GetFileName(file);
                                    var path = Path.Combine(
                                        Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, date, filename);
                                    string dbpath = user.UserName + "/" + date + "/" + filename;

                                    if (System.IO.File.Exists(file))
                                    {
                                        System.IO.File.Move(file, path);
                                        var photo = _db.PatientPhotos.FirstOrDefault(c => c.FileName == filename);
                                        photo.FilePath = dbpath;
                                        _db.Update(photo);
                                        await _db.SaveChangesAsync();

                                    }
                                }
                            }
                        }
                        change_userName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.FirstName.Trim()) && patientVM.FirstName.Trim() != patient.FirstName.Trim())
                    {
                        patient.FirstName = patientVM.FirstName.Trim();
                        change_firstName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.LastName.Trim()) && patientVM.LastName != patient.LastName.Trim())
                    {
                        patient.LastName = patientVM.LastName.Trim();
                        change_lastName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.Description) && patientVM.Description != patient.Description)
                    {
                        patient.Description = patientVM.Description;
                        change_description = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.PhoneNumber) && patientVM.PhoneNumber != user.PhoneNumber)
                    {
                        if (patientVM.PhoneNumber.Length == 9)
                        {
                            user.PhoneNumber = patientVM.PhoneNumber;
                            patient.PhoneNumber = patientVM.PhoneNumber;
                            change_phoneNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("phonenumber", "Numer telefonu musi się składać z 9 cyfr.");
                            return View(patientVM);
                        }
                    }

                    if (patientVM.RoomNumber != patient.RoomNumber)
                    {
                        if (patientVM.RoomNumber <= 999 && patientVM.RoomNumber >= 1)
                        {
                            patient.RoomNumber = patientVM.RoomNumber.GetValueOrDefault();
                            change_roomNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("roomnumber", "Niepoprawny numer pokoju.");
                            return View(patientVM);
                        }
                    }

                    var min_data = new DateTime(1900, 1, 1);
                    var default_data = new DateTime(0001, 01, 01);

                    if (patientVM.Date_of_birth != patient.Date_of_birth)
                    {
                        if (patientVM.Date_of_birth < DateTime.Now && patientVM.Date_of_birth >= min_data)
                        {
                            patient.Date_of_birth = patientVM.Date_of_birth;
                            change_dateOfBirth = true;
                        }
                        else
                        {
                            ModelState.AddModelError("date_of_birth", "Niepoprawna data urodzenia.");
                            return View(patientVM);
                        }
                    }

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(patientVM.Email) && patientVM.Email != user.Email)
                    {
                        user.Email = patientVM.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {
                            AddErrorsFromResult(validEmail);
                            return View(patientVM);
                        }
                        change_email = true;
                        patient.Email = patientVM.Email;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(patientVM.Password)) && (patientVM.Password == patientVM.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, patientVM.Password);
                        if (validPass.Succeeded)
                        {
                            user.PasswordHash = _passwordHasher.HashPassword(user, patientVM.Password);
                            change_password = true;
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                        }
                    }
                    else if (patientVM.Password != patientVM.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(patientVM);
                    }

                    if (change_userName || change_firstName || change_lastName || change_email || change_phoneNumber || change_password || change_roomNumber || change_description || change_dateOfBirth)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            patient.UpdateDate = DateTime.Now;
                            _db.Update(patient);
                            _db.SaveChanges();
                            return RedirectToAction("Patients");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
                }
           }

            return View(patientVM);
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
            {
                if (error.Code.Contains("Password"))
                {
                    ModelState.AddModelError("password", error.Description);
                }
                else if (error.Code.Contains("User"))
                {
                    ModelState.AddModelError("username", error.Description);
                }
                else if (error.Code.Contains("Email"))
                {
                    ModelState.AddModelError("email", error.Description);
                }
                else
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmPatient(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);
                if (patient == null)
                {
                    return NotFound();
                }

                patient.Confirm = true;
                _db.Update(patient);
                _db.SaveChanges();

                return RedirectToAction("Patients");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            var roles = await _userManager.GetRolesAsync(user);

            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    foreach (var role in roles)
                    {
                        if (role.Contains("Caregiver"))
                        {
                            var path_user = Path.Combine(
                                Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName);

                            if (Directory.Exists(path_user))
                            {
                                Directory.Delete(path_user, true);
                            }

                            return RedirectToAction("Caregivers");
                        }
                        else if (role.Contains("Patient"))
                        {
                            var path_user = Path.Combine(
                                Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName);
                            if (Directory.Exists(path_user))
                            {
                                Directory.Delete(path_user, true);
                            }
                            return RedirectToAction("Patients");
                        }
                        else
                        {
                            if (role == "Admin")
                            {
                                ModelState.AddModelError("", "Użytkownik nie może zostać usunięty.");
                            }
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Nie znaleziono użytkownika.");
                    //throw new NullReferenceException(nameof(userId));
                }
            }
            return View(user);
        }

        public IActionResult Caregivers()
        {
            var CaregiversVM = new CaregiversVM()
            {
                Title = "Opiekunowie",
                Caregivers = _db.Caregivers.OrderByDescending(o => o.AddDate).ToList()
            };
            return View(CaregiversVM);
        }

        public IActionResult Patients()
        {
            var PatientsVM = new PatientsVM()
            {
                Title = "Pacjenci",
                Patients = _db.Patients.OrderByDescending(o => o.AddDate).ToList()
            };
            return View(PatientsVM);
        }
    }
}