﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DementiaHelpApp.Data;
using DementiaHelpApp.Models;
using DementiaHelpApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Newtonsoft.Json.Linq;

namespace DementiaHelpApp.Controllers
{
    [Authorize(Roles = "Caregiver")]
    public class CaregiverController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly AppDbContext _db;
        private IUserValidator<User> _userValidator;
        private IPasswordValidator<User> _passwordValidator;
        private readonly IActionContextAccessor _accessor;
        private IPasswordHasher<User> _passwordHasher;

        public CaregiverController(UserManager<User> userManager, AppDbContext db, IUserValidator<User> userValidator, IPasswordValidator<User> passwordValidator, IPasswordHasher<User> passwordHasher, IActionContextAccessor accessor)
        {
            _userManager = userManager;
            _db = db;
            _userValidator = userValidator;
            _passwordValidator = passwordValidator;
            _passwordHasher = passwordHasher;
            _accessor = accessor;
        }

        [HttpGet]
        public IActionResult AddPhoto(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpGet]
        public IActionResult CreatePatient(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> CreateReminder(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);
                if (patient == null)
                {
                    return NotFound();
                }

                var PhotosVM = new PhotosVM()
                {
                    Title = "Przypominajka",
                    CaregiverPhotos = _db.CaregiverPhotos.OrderByDescending(o => o.AddDate).ToList(),
                    PatientPhotos = _db.PatientPhotos.Where(p => p.PatientId == patient.Id).OrderByDescending(o => o.AddDate).ToList()
                };
                return View(PhotosVM);
            }
            else
            {
                return RedirectToAction("Patients");
            }
        }

        [AllowAnonymous]
        [Route("przypominajka/pokoj/{room}")]
        [HttpGet]
        public IActionResult Room(int room)
        {
            var ip = _accessor.ActionContext.HttpContext.Connection.RemoteIpAddress.ToString();
            var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "ip_server.txt");
            string ip_server = null;

            if (System.IO.File.Exists(path))
            {
                ip_server = System.IO.File.ReadAllText(path);
            }

            if(!(ip.StartsWith(ip_server) || ip.Contains("::1"))) 
            {
                return NotFound();
            }

            var patients = _db.Patients.Where(p => p.RoomNumber == room).ToList();
            if (patients.Count == 0)
            {
                return NotFound();
            }
            else if (patients.Count > 0)
            {
                List<string> names = new List<string>();
                List<List<PatientPhoto>> photos = new List<List<PatientPhoto>>();
                foreach (var patient in patients)
                {
                    List<PatientPhoto> photo = _db.PatientPhotos.Where(p => p.PatientId == patient.Id).OrderBy(o => o.AddDate).ToList();
                    if(photo.Any())
                    {
                        photos.Add(photo);
                        string name = patient.FirstName + " " + patient.LastName;
                        names.Add(name);
                    }                    
                }
                var PhotoVM = new ManyPatientPhotosVM()
                {
                    Title = "Pokój numer " + room.ToString(),
                    PatientName = names,
                    CaregiverPhotos = _db.CaregiverPhotos.OrderBy(o => o.AddDate).ToList(),
                    PatientPhotos = photos
                };
                return View(PhotoVM);
            }
            else
                return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Patients()
        {
            var PatientsVM = new PatientsVM()
            {
                Title = "Pacjenci",
                Patients = _db.Patients.OrderByDescending(o => o.AddDate).ToList()
            };
            return View(PatientsVM);
        }

        [HttpGet]
        public async Task<IActionResult> Photos(string userId, EditPhotoVM photoVM)
        {
            User user = await _userManager.FindByIdAsync(userId);
            var caregiver = _db.Caregivers.FirstOrDefault(p => p.UserId == user.Id);
            var PhotosVM = new CaregiverPhotosVM()
            {
                Title = "Zdjęcia",
                Photos = _db.CaregiverPhotos.Where(p => p.CaregiverId == caregiver.Id).OrderByDescending(o => o.AddDate).ToList(),
                PhotoVM = photoVM
            };
            return View(PhotosVM);
        }

        [HttpGet]
        public async Task<IActionResult> Update(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var caregiver = _db.Caregivers.FirstOrDefault(c => c.UserId == user.Id);
                if (caregiver == null)
                {
                    return NotFound();
                }

                EditCaregiverVM caregiverVM = new EditCaregiverVM { UserName = user.UserName, Email = caregiver.Email, PhoneNumber = caregiver.PhoneNumber, FirstName = caregiver.FirstName, LastName = caregiver.LastName, Description = caregiver.Description, UserId = user.Id };

                return View(caregiverVM);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public async Task<IActionResult> UpdatePatient(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);
                if (patient == null)
                {
                    return NotFound();
                }
                EditPatientVM patientVM = new EditPatientVM { UserName = user.UserName, Email = patient.Email, FirstName = patient.FirstName, LastName = patient.LastName, PhoneNumber = patient.PhoneNumber, Date_of_birth = patient.Date_of_birth, RoomNumber = patient.RoomNumber, Description = patient.Description, UserId = user.Id };

                return View(patientVM);
            }
            else
            {
                return RedirectToAction("Patients");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPhoto(string userId, PhotoVM photoVM)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(userId);
                Caregiver caregiver = _db.Caregivers.FirstOrDefault(p => p.UserId == user.Id);

                if (caregiver == null)
                {
                    return NotFound();
                }

                if (photoVM.FormFile == null && photoVM.FormFile.Length == 0)
                {
                    ModelState.AddModelError("", "Nie dodano żadnego zdjęcia.");
                    return View(photoVM);
                }

                var photos = _db.CaregiverPhotos.Where(p => p.CaregiverId == caregiver.Id).ToList();
                if (photos.Count > 10)
                {
                    ModelState.AddModelError("", "Nie można dodać kolejnego zdjęcia. Limit do 10 zdjęć.");
                    return View(photoVM);
                }

                if (photoVM.FormFile.Length > 1024 * 1024 * 4)
                {
                    ModelState.AddModelError("", "Rozmiar zdjęcia nie może być większy niż 4MB.");
                    return View(photoVM);
                }

                if (photoVM.FormFile.Length < 100)
                {
                    ModelState.AddModelError("", "Rozmiar zdjęcia jest za mały.");
                    return View(photoVM);
                }

                string extension = Path.GetExtension(photoVM.FormFile.FileName).ToLower();

                if (extension != ".jpg" && extension != ".jpeg" && extension != ".png" && extension != ".gif" && extension != ".tiff")
                {
                    ModelState.AddModelError("", "Obsługiwane rozszerzenia plików: jpg, jpeg, png, gif, tiff.");
                    return View(photoVM);
                }

                if (photoVM.FormFile != null && photoVM.FormFile.Length > 0)
                {
                    string date = DateTime.Now.ToString("dd.MM.yyyy");
                    string guid = Guid.NewGuid().ToString();
                    string filename = guid + Path.GetRandomFileName() + Path.GetExtension(photoVM.FormFile.FileName);
                    var path_username = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName);
                    if (!System.IO.File.Exists(path_username))
                    {
                        Directory.CreateDirectory(path_username);
                    }

                    var path_date = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName, date);
                    if (!System.IO.File.Exists(path_date))
                    {
                        Directory.CreateDirectory(path_date);
                    }
                    var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName, date, filename);

                    string dbpath = user.UserName + "/" + date + "/" + filename;

                    if (!System.IO.File.Exists(path))
                    {
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await photoVM.FormFile.CopyToAsync(stream);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Plik o podanej nazwie już istnieje.");
                        return View(photoVM);
                    }

                    CaregiverPhoto photo = new CaregiverPhoto { AddDate = DateTime.Now, FileName = filename, FileLength = (int)photoVM.FormFile.Length, FileType = extension, FilePath = dbpath, Caregiver = caregiver, CaregiverId = caregiver.Id, Description = photoVM.Description };
                    _db.Add(photo);
                    await _db.SaveChangesAsync();
                }

                return RedirectToAction("Photos", new { userId });
            }
            else
                return View(photoVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmPatient(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);
                if (patient == null)
                {
                    return NotFound();
                }

                patient.Confirm = true;
                _db.Update(patient);
                _db.SaveChanges();

                return RedirectToAction("Patients");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatePatient(CreatePatientVM patientVM)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(patientVM.UserName);
                if (user == null)
                {
                    user = new User()
                    {
                        UserName = patientVM.UserName,
                        Email = patientVM.Email,
                        AddDate = DateTime.Now
                    };
                    if (patientVM.Password == patientVM.ConfirmPassword)
                    {
                        var min_data = new DateTime(1900, 1, 1);
                        var default_data = new DateTime(0001, 01, 01);

                        if (patientVM.Date_of_birth < DateTime.Now && patientVM.Date_of_birth >= min_data)
                        {
                            var result = await _userManager.CreateAsync(user, patientVM.Password);

                            if (result.Succeeded)
                            {
                                patientVM.SetRole();
                                await _userManager.AddToRoleAsync(user, patientVM.Roles.ToString());
                                await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, patientVM.Roles.ToString()));

                                if (patientVM.Roles.ToString().Contains("Patient"))
                                {

                                    if (_db.Caregivers.Any())
                                    {

                                        Patient patient = new Patient { AddDate = user.AddDate, Email = user.Email, FirstName = patientVM.FirstName.Trim(), LastName = patientVM.LastName.Trim(), Date_of_birth = patientVM.Date_of_birth.GetValueOrDefault(), RoomNumber = patientVM.RoomNumber.GetValueOrDefault(), Description = patientVM.Description, Confirm = true, UserId = user.Id, User = user };
                                        _db.Patients.Add(patient);
                                        _db.SaveChanges();

                                        if (_db.Caregivers.Any())
                                        {
                                            Patient copypatient = _db.Patients.FirstOrDefault(a => a.Id == patient.Id);
                                            foreach (var caregiver in _db.Caregivers)
                                            {
                                                _db.CaregiverPatients.Add(new CaregiverPatient { Caregiver = caregiver, Patient = copypatient });
                                            }
                                            _db.SaveChanges();
                                        }

                                        return RedirectToAction("Patients");
                                    }
                                    else
                                    {
                                        IdentityResult unresult = await _userManager.DeleteAsync(user);
                                        return RedirectToAction("Patients");
                                    }


                                }

                            }
                            else
                            {
                                var list_of_errors = result.Errors.ToList();
                                foreach (var error in list_of_errors)
                                {
                                    if (error.Code.Contains("Password"))
                                    {
                                        ModelState.AddModelError("password", error.Description);
                                    }
                                    else if (error.Code.Contains("User"))
                                    {
                                        ModelState.AddModelError("username", error.Description);
                                    }
                                    else if (error.Code.Contains("Email"))
                                    {
                                        ModelState.AddModelError("email", error.Description);
                                    }
                                }
                            }
                        }
                        else
                            ModelState.AddModelError("date_of_birth", "Niepoprawna data urodzenia.");
                    }
                    else
                        ModelState.AddModelError("password", "Podane hasło i powtórzone hasło musi się zgadzać.");
                }
                else
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
            }
            return View(patientVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            var roles = await _userManager.GetRolesAsync(user);

            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    foreach (var role in roles)
                    {
                        if (role.Contains("Patient"))
                        {
                            var path_user = Path.Combine(
                                Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName);
                            if (Directory.Exists(path_user))
                            {
                                Directory.Delete(path_user, true);
                            }

                            return RedirectToAction("Patients");
                        }
                        else
                        {
                            if (role == "Admin" || role.Contains("Caregiver"))
                            {
                                ModelState.AddModelError("", "Użytkownik nie może zostać usunięty.");
                            }
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Nie znaleziono użytkownika.");
                    //throw new NullReferenceException(nameof(userId));
                }
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeletePhoto(string photoId)
        {
            CaregiverPhoto photo = _db.CaregiverPhotos.FirstOrDefault(p => p.Id == photoId);
            string userId = _db.Caregivers.FirstOrDefault(p => p.Id == photo.CaregiverId).UserId;
            User user = _db.Users.FirstOrDefault(u => u.Id == userId);
            var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName, photo.AddDate.ToString("dd.MM.yyyy"), photo.FileName);
            var path_date = Path.Combine(
                   Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName, photo.AddDate.ToString("dd.MM.yyyy"));


            if (photo != null)
            {
                _db.CaregiverPhotos.Remove(photo);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    string[] dates = Directory.GetFiles(path_date);
                    if (dates.Length == 0)
                    {
                        System.IO.File.Delete(path);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Plik o podanej nazwie już nie istnieje.");
                }
                await _db.SaveChangesAsync();
            }
            else
            {
                ModelState.AddModelError("", "Nie znaleziono zdjęcia.");
            }

            return RedirectToAction("Photos", new { userId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(EditCaregiverVM caregiverVM)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(caregiverVM.UserId);
                bool change_userName = false;
                bool change_firstName = false;
                bool change_lastName = false;
                bool change_phoneNumber = false;
                bool change_email = false;
                bool change_password = false;

                if (user != null)
                {
                    var caregiver = _db.Caregivers.FirstOrDefault(p => p.UserId == user.Id);
                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);

                    if (!string.IsNullOrEmpty(caregiverVM.UserName) && caregiverVM.UserName != user.UserName)
                    {
                        var old_userName = user.UserName;
                        user.UserName = caregiverVM.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(caregiverVM);
                        }

                        var old_path_user = Path.Combine(
                                Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", old_userName);

                        var path_username = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName);
                        if (!System.IO.File.Exists(path_username))
                        {
                            Directory.CreateDirectory(path_username);
                        }
                        if (System.IO.Directory.Exists(old_path_user))
                        {
                            string[] dates = Directory.GetFileSystemEntries(old_path_user);
                            foreach (var item in dates)
                            {
                                var date = Path.GetFileName(item);
                                var path_date = Path.Combine(
                                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName, date);
                                if (!System.IO.File.Exists(path_date))
                                {
                                    Directory.CreateDirectory(path_date);
                                }
                                string[] files = Directory.GetFiles(item);
                                foreach (var file in files)
                                {
                                    var filename = Path.GetFileName(file);
                                    var path = Path.Combine(
                                        Directory.GetCurrentDirectory(), "wwwroot", "photos", "caregivers", user.UserName, date, filename);
                                    string dbpath = user.UserName + "/" + date + "/" + filename;

                                    if (System.IO.File.Exists(file))
                                    {
                                        System.IO.File.Move(file, path);
                                        var photo = _db.CaregiverPhotos.FirstOrDefault(c => c.FileName == filename);
                                        photo.FilePath = dbpath;
                                        _db.Update(photo);
                                        await _db.SaveChangesAsync();

                                    }
                                }
                            }
                        }
                        change_userName = true;
                    }

                    if (!string.IsNullOrEmpty(caregiverVM.FirstName.Trim()) && caregiverVM.FirstName.Trim() != caregiver.FirstName.Trim())
                    {
                        caregiver.FirstName = caregiverVM.FirstName.Trim();
                        change_firstName = true;
                    }

                    if (!string.IsNullOrEmpty(caregiverVM.LastName.Trim()) && caregiverVM.LastName.Trim() != caregiver.LastName.Trim())
                    {
                        caregiver.LastName = caregiverVM.LastName.Trim();
                        change_lastName = true;
                    }

                    if (!string.IsNullOrEmpty(caregiverVM.PhoneNumber) && caregiverVM.PhoneNumber != user.PhoneNumber)
                    {
                        if (caregiverVM.PhoneNumber.Length == 9)
                        {
                            user.PhoneNumber = caregiverVM.PhoneNumber;
                            caregiver.PhoneNumber = caregiverVM.PhoneNumber;
                            change_phoneNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("phonenumber", "Numer telefonu musi się składać z 9 cyfr.");
                            return View(caregiverVM);
                        }

                    }

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(caregiverVM.Email) && caregiverVM.Email != user.Email)
                    {
                        user.Email = caregiverVM.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {
                            AddErrorsFromResult(validEmail);
                            return View(caregiverVM);
                        }
                        change_email = true;
                        caregiver.Email = caregiverVM.Email;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(caregiverVM.Password)) && (!string.IsNullOrEmpty(caregiverVM.ConfirmPassword)) && (!string.IsNullOrEmpty(caregiverVM.OldPassword)) && (caregiverVM.OldPassword != caregiverVM.Password) && (caregiverVM.Password == caregiverVM.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, caregiverVM.Password);
                        if (validPass.Succeeded)
                        {
                            var result = _userManager.ChangePasswordAsync(user, caregiverVM.OldPassword, caregiverVM.Password);
                            if (result.Result.Succeeded)
                            {
                                user.PasswordHash = _passwordHasher.HashPassword(user, caregiverVM.Password);
                                change_password = true;
                            }
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                            return View(caregiverVM);
                        }
                    }
                    else if ((!string.IsNullOrEmpty(caregiverVM.OldPassword)) && (!string.IsNullOrEmpty(caregiverVM.Password)) && (caregiverVM.OldPassword == caregiverVM.Password))
                    {
                        ModelState.AddModelError("oldpassword", "Stare hasło i nowe hasło musi być różne.");
                        return View(caregiverVM);
                    }
                    else if (caregiverVM.Password != caregiverVM.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(caregiverVM);
                    }


                    if (change_userName || change_phoneNumber || change_email || change_password || change_firstName || change_lastName)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            caregiver.UpdateDate = DateTime.Now;
                            _db.Update(caregiver);
                            _db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                            return View(caregiverVM);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
                }
            }

            return View(caregiverVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdatePatient(EditPatientVM patientVM)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(patientVM.UserId);
                bool change_userName = false;
                bool change_firstName = false;
                bool change_lastName = false;
                bool change_description = false;
                bool change_phoneNumber = false;
                bool change_email = false;
                bool change_password = false;
                bool change_roomNumber = false;
                bool change_dateOfBirth = false;

                if (user != null)
                {
                    var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);

                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);

                    if (!string.IsNullOrEmpty(patientVM.UserName) && patientVM.UserName != user.UserName)
                    {
                        var old_userName = user.UserName;
                        user.UserName = patientVM.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(patientVM);
                        }

                        var old_path_user = Path.Combine(
                                Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", old_userName);

                        var path_username = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName);
                        if (!System.IO.File.Exists(path_username))
                        {
                            Directory.CreateDirectory(path_username);
                        }
                        if (System.IO.Directory.Exists(old_path_user))
                        {
                            string[] dates = Directory.GetFileSystemEntries(old_path_user);
                            foreach (var item in dates)
                            {
                                var date = Path.GetFileName(item);
                                var path_date = Path.Combine(
                                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, date);
                                if (!System.IO.File.Exists(path_date))
                                {
                                    Directory.CreateDirectory(path_date);
                                }
                                string[] files = Directory.GetFiles(item);
                                foreach (var file in files)
                                {
                                    var filename = Path.GetFileName(file);
                                    var path = Path.Combine(
                                        Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, date, filename);
                                    string dbpath = user.UserName + "/" + date + "/" + filename;

                                    if (System.IO.File.Exists(file))
                                    {
                                        System.IO.File.Move(file, path);
                                        var photo = _db.PatientPhotos.FirstOrDefault(c => c.FileName == filename);
                                        photo.FilePath = dbpath;
                                        _db.Update(photo);
                                        await _db.SaveChangesAsync();

                                    }
                                }
                            }
                        }
                        change_userName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.FirstName) && patientVM.FirstName != patient.FirstName)
                    {
                        patient.FirstName = patientVM.FirstName;
                        change_firstName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.LastName) && patientVM.LastName != patient.LastName)
                    {
                        patient.LastName = patientVM.LastName;
                        change_lastName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.Description) && patientVM.Description != patient.Description)
                    {
                        patient.Description = patientVM.Description;
                        change_description = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.PhoneNumber) && patientVM.PhoneNumber != user.PhoneNumber)
                    {
                        if (patientVM.PhoneNumber.Length == 9)
                        {
                            user.PhoneNumber = patientVM.PhoneNumber;
                            patient.PhoneNumber = patientVM.PhoneNumber;
                            change_phoneNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("phonenumber", "Numer telefonu musi się składać z 9 cyfr.");
                            return View(patientVM);
                        }
                    }

                    if (patientVM.RoomNumber != patient.RoomNumber)
                    {
                        if (patientVM.RoomNumber <= 999 && patientVM.RoomNumber >= 1)
                        {
                            patient.RoomNumber = patientVM.RoomNumber.GetValueOrDefault();
                            change_roomNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("roomnumber", "Niepoprawny numer pokoju.");
                            return View(patientVM);
                        }
                    }

                    var min_data = new DateTime(1900, 1, 1);
                    var default_data = new DateTime(0001, 01, 01);

                    if (patientVM.Date_of_birth != patient.Date_of_birth)
                    {
                        if (patientVM.Date_of_birth < DateTime.Now && patientVM.Date_of_birth >= min_data)
                        {
                            patient.Date_of_birth = patientVM.Date_of_birth;
                            change_dateOfBirth = true;
                        }
                        else
                        {
                            ModelState.AddModelError("date_of_birth", "Niepoprawna data urodzenia.");
                            return View(patientVM);
                        }
                    }

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(patientVM.Email) && patientVM.Email != user.Email)
                    {
                        user.Email = patientVM.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {
                            AddErrorsFromResult(validEmail);
                            return View(patientVM);
                        }
                        change_email = true;
                        patient.Email = patientVM.Email;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(patientVM.Password)) && (patientVM.Password == patientVM.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, patientVM.Password);
                        if (validPass.Succeeded)
                        {
                            user.PasswordHash = _passwordHasher.HashPassword(user, patientVM.Password);
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                        }
                    }
                    else if (patientVM.Password != patientVM.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(patientVM);
                    }

                    if (change_userName || change_firstName || change_lastName || change_email || change_phoneNumber || change_password || change_roomNumber || change_description || change_dateOfBirth)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            patient.UpdateDate = DateTime.Now;
                            _db.Update(patient);
                            _db.SaveChanges();
                            return RedirectToAction("Patients");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
                }
            }

            return View(patientVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdatePhoto(string photoId, EditPhotoVM photoVM)
        {
            CaregiverPhoto photo = _db.CaregiverPhotos.FirstOrDefault(p => p.Id == photoId);
            string userId = _db.Caregivers.FirstOrDefault(p => p.Id == photo.CaregiverId).UserId;

            if (ModelState.IsValid)
            {
                if (photo != null && !string.IsNullOrEmpty(photo.Description))
                {
                    photo.Description = photoVM.Description;
                    _db.Update(photo);
                    await _db.SaveChangesAsync();

                }
                else
                {
                    ModelState.AddModelError("", "Uzupełnij opis.");
                }

                return RedirectToAction("Photos", new { userId });
            }
            else
                return RedirectToAction("Photos", new { userId, photoVM });
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
            {
                if (error.Code.Contains("Password"))
                {
                    ModelState.AddModelError("password", error.Description);
                }
                else if (error.Code.Contains("User"))
                {
                    ModelState.AddModelError("username", error.Description);
                }
                else if (error.Code.Contains("Email"))
                {
                    ModelState.AddModelError("email", error.Description);
                }
                else
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
        }
    }

}
