﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DementiaHelpApp.Data;
using DementiaHelpApp.Models;
using DementiaHelpApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DementiaHelpApp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<User> _logger;
        private readonly AppDbContext _db;

        public AccountController(SignInManager<User> signInManager, UserManager<User> userManager, ILogger<User> logger, AppDbContext db)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
            _db = db;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Log
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginVM model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.UserName);

                if (user != null)
                {
                    await _signInManager.SignOutAsync();
                    var result = await _signInManager.PasswordSignInAsync(model.UserName,
                     model.Password, model.RememberMe, false);

                    if (result.Succeeded)
                    {
                        var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);
                        if (await _userManager.IsInRoleAsync(user, "Admin") || await _userManager.IsInRoleAsync(user, "Caregiver") || patient.Confirm)
                        {
                            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                            {
                                return Redirect(returnUrl ?? "/");
                            }
                            else
                            {
                                if (await _userManager.IsInRoleAsync(user, "Admin"))
                                {
                                    return RedirectToAction("Index", "Admin");
                                }
                                else if (await _userManager.IsInRoleAsync(user, "Caregiver"))
                                {
                                    return RedirectToAction("Index", "Caregiver");
                                }
                                else
                                    return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                            ModelState.AddModelError("", "Koto użytkownika musi zostać potwierdzone.");
                    }
                    else
                        ModelState.AddModelError("", "Nazwa użytkownika lub hasło jest nieprawidłowe.");
                }
                else
                    ModelState.AddModelError("", "Nazwa użytkownika lub hasło jest nieprawidłowe.");
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
        #endregion

        #region Register
        [AllowAnonymous]
        public IActionResult Register()
        { 
            return View(new RegisterVM());
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Remember()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterVM model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.UserName);
                if (user == null)
                {
                    user = new User() { UserName = model.UserName, PhoneNumber = model.PhoneNumber, AddDate = DateTime.Now };
                    if(model.Password==model.ConfirmPassword)
                    {
                        var result = await _userManager.CreateAsync(user, model.Password);
                        if (result.Succeeded)
                        {
                            await _userManager.AddToRoleAsync(user, "Patient");
                            await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, "Patient"));
                            if (_db.Caregivers.Any())
                            {

                                Patient patient = new Patient { AddDate = user.AddDate, Email = user.Email, FirstName = user.UserName, UserId = user.Id, User = user, Confirm = false };
                                _db.Patients.Add(patient);
                                _db.SaveChanges();

                                if (_db.Caregivers.Any())
                                {
                                    Patient copypatient = _db.Patients.FirstOrDefault(a => a.Id == patient.Id);
                                    foreach (var caregiver in _db.Caregivers)
                                    {
                                        _db.CaregiverPatients.Add(new CaregiverPatient { Caregiver = caregiver, Patient = copypatient });
                                    }
                                    _db.SaveChanges();
                                }
                            }
                                return RedirectToAction("Index", "Account");
                        }
                        else
                        {
                            var list_of_errors = result.Errors.ToList();
                            foreach (var error in list_of_errors)
                            {
                                if (error.Code.Contains("Password"))
                                {
                                    ModelState.AddModelError("password", error.Description);
                                }
                                else if (error.Code.Contains("User"))
                                {
                                    ModelState.AddModelError("username", error.Description);
                                }
                                else
                                    ModelState.AddModelError("", error.Description);
                            }
                        }
                    }
                    else
                        ModelState.AddModelError("password", "Podane hasło i powtórzone hasło muszą się zgadzać.");
                }
                else
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
            }

            return View(model);
        }
        #endregion
    }
}