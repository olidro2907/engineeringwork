﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DementiaHelpApp.Models;
using DementiaHelpApp.Data;
using Microsoft.AspNetCore.Identity;
using DementiaHelpApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Security.Policy;

namespace DementiaHelpApp.Controllers
{
    [Authorize(Roles = "Patient")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<User> _userManager;
        private readonly AppDbContext _db;
        private IUserValidator<User> _userValidator;
        private IPasswordValidator<User> _passwordValidator;
        private IPasswordHasher<User> _passwordHasher;

        public HomeController(ILogger<HomeController> logger, UserManager<User> userManager, AppDbContext db, IUserValidator<User> userValidator, IPasswordValidator<User> passwordValidator, IPasswordHasher<User> passwordHasher)
        {
            _logger = logger;
            _userManager = userManager;
            _db = db;
            _userValidator = userValidator;
            _passwordValidator = passwordValidator;
            _passwordHasher = passwordHasher;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Photos(string userId, EditPhotoVM photoVM)
        {
            User user = await _userManager.FindByIdAsync(userId);
            var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);
            var PhotosVM = new PatientPhotosVM()
            {
                Title = "Zdjęcia",
                Photos = _db.PatientPhotos.Where(p => p.PatientId == patient.Id).OrderByDescending(o => o.AddDate).ToList(),
                PhotoVM = photoVM
            };
            return View(PhotosVM);
        }

        [HttpGet]
        public async Task<IActionResult> Update(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);
                if (patient == null)
                {
                    return NotFound();
                }
                EditPatientVM patientVM = new EditPatientVM { UserName = user.UserName, Email = patient.Email, FirstName = patient.FirstName, LastName = patient.LastName, PhoneNumber = patient.PhoneNumber, Date_of_birth = patient.Date_of_birth, RoomNumber = patient.RoomNumber, Description = patient.Description, UserId = user.Id };

                return View(patientVM);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public IActionResult AddPhoto(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(EditPatientVM patientVM)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(patientVM.UserId);
                bool change_userName = false;
                bool change_firstName = false;
                bool change_lastName = false;
                bool change_phoneNumber = false;
                bool change_email = false;
                bool change_password = false;
                bool change_roomNumber = false;
                bool change_dateOfBirth = false;

                if (user != null)
                {
                    var patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);

                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);

                    if (!string.IsNullOrEmpty(patientVM.UserName) && patientVM.UserName != user.UserName)
                    {
                        var old_userName = user.UserName;
                        user.UserName = patientVM.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(patientVM);
                        }

                        var old_path_user = Path.Combine(
                                Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", old_userName);

                        var path_username = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName);
                        if (!System.IO.File.Exists(path_username))
                        {
                            Directory.CreateDirectory(path_username);
                        }
                        if(System.IO.Directory.Exists(old_path_user))
                        {
                            string[] dates = Directory.GetFileSystemEntries(old_path_user);
                            foreach (var item in dates)
                            {
                                var date = Path.GetFileName(item);
                                var path_date = Path.Combine(
                                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, date);
                                if (!System.IO.File.Exists(path_date))
                                {
                                    Directory.CreateDirectory(path_date);
                                }
                                string[] files = Directory.GetFiles(item);
                                foreach (var file in files)
                                {
                                    var filename = Path.GetFileName(file);
                                    var path = Path.Combine(
                                        Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, date, filename);
                                    string dbpath = user.UserName + "/" + date + "/" + filename;

                                    if (System.IO.File.Exists(file))
                                    {
                                        System.IO.File.Move(file, path);
                                        var photo = _db.PatientPhotos.FirstOrDefault(c => c.FileName == filename);
                                        photo.FilePath = dbpath;
                                        _db.Update(photo);
                                        await _db.SaveChangesAsync();

                                    }
                                }
                            }
                        }
                        change_userName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.FirstName.Trim()) && patientVM.FirstName.Trim() != patient.FirstName.Trim())
                    {
                        patient.FirstName = patientVM.FirstName.Trim();
                        change_firstName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.LastName.Trim()) && patientVM.LastName.Trim() != patient.LastName.Trim())
                    {
                        patient.LastName = patientVM.LastName.Trim();
                        change_lastName = true;
                    }

                    if (!string.IsNullOrEmpty(patientVM.PhoneNumber) && patientVM.PhoneNumber != user.PhoneNumber)
                    {
                        if (patientVM.PhoneNumber.Length == 9)
                        {
                            user.PhoneNumber = patientVM.PhoneNumber;
                            patient.PhoneNumber = patientVM.PhoneNumber;
                            change_phoneNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("phonenumber", "Numer telefonu musi się składać z 9 cyfr.");
                            return View(patientVM);
                        }
                    }

                    if (patientVM.RoomNumber != patient.RoomNumber)
                    {
                        if (patientVM.RoomNumber <= 999 && patientVM.RoomNumber >= 1)
                        {
                            patient.RoomNumber = patientVM.RoomNumber.GetValueOrDefault();
                            change_roomNumber = true;
                        }
                        else
                        {
                            ModelState.AddModelError("roomnumber", "Niepoprawny numer pokoju.");
                            return View(patientVM);
                        }
                    }

                    var min_data = new DateTime(1900, 1, 1);
                    var default_data = new DateTime(0001, 01, 01);

                    if (patientVM.Date_of_birth != patient.Date_of_birth)
                    {
                        if (patientVM.Date_of_birth < DateTime.Now && patientVM.Date_of_birth >= min_data)
                        {
                            patient.Date_of_birth = patientVM.Date_of_birth;
                            change_dateOfBirth = true;
                        }
                        else
                        {
                            ModelState.AddModelError("date_of_birth", "Niepoprawna data urodzenia.");
                            return View(patientVM);
                        }
                    }

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(patientVM.Email) && patientVM.Email != user.Email)
                    {
                        user.Email = patientVM.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {
                            AddErrorsFromResult(validEmail);
                            return View(patientVM);
                        }
                        change_email = true;
                        patient.Email = patientVM.Email;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(patientVM.Password)) && (patientVM.Password == patientVM.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, patientVM.Password);
                        if (validPass.Succeeded)
                        {
                            user.PasswordHash = _passwordHasher.HashPassword(user, patientVM.Password);
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                        }
                    }
                    else if (patientVM.Password != patientVM.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(patientVM);
                    }

                    if (change_userName || change_firstName || change_lastName || change_email || change_phoneNumber || change_password || change_roomNumber || change_dateOfBirth)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            patient.UpdateDate = DateTime.Now;
                            _db.Update(patient);
                            _db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
                }
            }

            return View(patientVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdatePhoto(string photoId, EditPhotoVM photoVM)
        {
            PatientPhoto photo = _db.PatientPhotos.FirstOrDefault(p => p.Id == photoId);
            string userId = _db.Patients.FirstOrDefault(p => p.Id == photo.PatientId).UserId;

            if (ModelState.IsValid)
            {
                if (photo != null && !string.IsNullOrEmpty(photo.Description))
                {
                    photo.Description = photoVM.Description;
                    _db.Update(photo);
                    await _db.SaveChangesAsync();

                }
                else
                {
                    ModelState.AddModelError("", "Uzupełnij opis.");
                }

                return RedirectToAction("Photos", new { userId });
            }
            else
                return RedirectToAction("Photos", new { userId, photoVM });
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
            {
                if (error.Code.Contains("Password"))
                {
                    ModelState.AddModelError("password", error.Description);
                }
                else if (error.Code.Contains("User"))
                {
                    ModelState.AddModelError("username", error.Description);
                }
                else if (error.Code.Contains("Email"))
                {
                    ModelState.AddModelError("email", error.Description);
                }
                else
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddPhoto(string userId, PhotoVM photoVM)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(userId);
                Patient patient = _db.Patients.FirstOrDefault(p => p.UserId == user.Id);

                if (patient == null)
                {
                    return NotFound();
                }

                if (photoVM.FormFile == null && photoVM.FormFile.Length == 0)
                {
                    ModelState.AddModelError("", "Nie dodano żadnego zdjęcia.");
                    return View(photoVM);
                }

                var photos = _db.PatientPhotos.Where(p => p.PatientId == patient.Id).ToList();
                if(photos.Count>12)
                {
                    ModelState.AddModelError("", "Nie można dodać kolejnego zdjęcia. Limit do 12 zdjęć.");
                    return View(photoVM);
                }

                if (photoVM.FormFile.Length > 1024 * 1024 * 4)
                {
                    ModelState.AddModelError("", "Rozmiar zdjęcia nie może być większy niż 4MB.");
                    return View(photoVM);
                }

                if (photoVM.FormFile.Length < 100)
                {
                    ModelState.AddModelError("", "Rozmiar zdjęcia jest za mały.");
                    return View(photoVM);
                }

                string extension = Path.GetExtension(photoVM.FormFile.FileName).ToLower();

                if (extension != ".jpg" && extension != ".jpeg" && extension != ".png" && extension != ".gif" && extension != ".tiff")
                {
                    ModelState.AddModelError("", "Obsługiwane rozszerzenia plików: jpg, jpeg, png, gif, tiff.");
                    return View(photoVM);
                }

                if (photoVM.FormFile != null && photoVM.FormFile.Length > 0)
                {
                    string date = DateTime.Now.ToString("dd.MM.yyyy");
                    string guid = Guid.NewGuid().ToString();
                    string filename = guid + Path.GetRandomFileName() + Path.GetExtension(photoVM.FormFile.FileName);
                    var path_username = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName);
                    if (!System.IO.File.Exists(path_username))
                    {
                        Directory.CreateDirectory(path_username);
                    }

                    var path_date = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, date);
                    if (!System.IO.File.Exists(path_date))
                    {
                        Directory.CreateDirectory(path_date);
                    }
                    var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, date, filename);

                    string dbpath = user.UserName + "/" + date + "/" + filename;

                    if (!System.IO.File.Exists(path))
                    {
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await photoVM.FormFile.CopyToAsync(stream);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Plik o podanej nazwie już istnieje.");
                        return View(photoVM);
                    }

                    PatientPhoto photo = new PatientPhoto { AddDate = DateTime.Now, FileName = filename, FileLength = (int)photoVM.FormFile.Length, FileType = extension, FilePath = dbpath, Patient = patient, PatientId = patient.Id, Description = photoVM.Description };
                    _db.Add(photo);
                    await _db.SaveChangesAsync();
                }

                return RedirectToAction("Photos", new { userId });
            }
            else
                return View(photoVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeletePhoto(string photoId)
        {
            PatientPhoto photo = _db.PatientPhotos.FirstOrDefault(p => p.Id == photoId);
            string userId = _db.Patients.FirstOrDefault(p => p.Id == photo.PatientId).UserId;
            User user = _db.Users.FirstOrDefault(u => u.Id == userId);
            var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, photo.AddDate.ToString("dd.MM.yyyy"), photo.FileName);
            var path_date = Path.Combine(
                   Directory.GetCurrentDirectory(), "wwwroot", "photos", "patients", user.UserName, photo.AddDate.ToString("dd.MM.yyyy"));

            if (photo != null)
            {
                _db.PatientPhotos.Remove(photo);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    string[] dates = Directory.GetFiles(path_date);
                    if (dates.Length == 0)
                    {
                        System.IO.File.Delete(path);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Plik o podanej nazwie już nie istnieje.");
                }
                await _db.SaveChangesAsync();
            }
            else
            {
                ModelState.AddModelError("", "Nie znaleziono zdjęcia.");
            }

            return RedirectToAction("Photos", new { userId });
        }

        private byte[] ConvertToBytes(IFormFile image)
        {
            using (var memoryStream = new MemoryStream())
            {
                image.OpenReadStream().CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }
    }
}
