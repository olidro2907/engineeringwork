﻿using DementiaHelpApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.Data
{
    public class AppDbContext: IdentityDbContext<User>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<CaregiverPatient>().HasKey(x => new { x.CaregiverId, x.PatientId });
        }

        public DbSet<Caregiver> Caregivers { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientPhoto> PatientPhotos { get; set; }
        public DbSet<CaregiverPhoto> CaregiverPhotos { get; set; }
        public DbSet<Reminder> Reminders { get; set; }
        public DbSet<CaregiverPatient> CaregiverPatients { get; set; }
    }
}
