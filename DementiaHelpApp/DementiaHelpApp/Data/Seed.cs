﻿using DementiaHelpApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DementiaHelpApp.Data
{
    public static class Seed
    {
        public static async Task CreateUserRoles(IServiceProvider serviceProvider)
        {
            // Initializing custom roles   
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<User>>();
            string[] roleNames = { "Caregiver", "Patient" };
            IdentityResult roleResult;

            // Adding Admin, Caregiver and User Role
            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //Create the roles and seed them to the database 
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            // Assign Admin role to newly registered user
            User admin = await UserManager.FindByEmailAsync("olidro2907@gmail.com");
            if (admin == null)
            {
                admin = new User()
                {
                    UserName = "admin",
                    Email = "olidro2907@gmail.com",
                    AddDate = DateTime.Now
                };
                await UserManager.CreateAsync(admin, "1wsxZAQ2");
            }

            var roleCheck = await RoleManager.RoleExistsAsync("Admin");
            IdentityRole adminrole = new IdentityRole();
            adminrole.Name = "Admin";

            if (!roleCheck)
            {
                roleResult = await RoleManager.CreateAsync(adminrole);

                await RoleManager.AddClaimAsync(adminrole, new Claim("Can add roles", "add.role"));
                await RoleManager.AddClaimAsync(adminrole, new Claim("Can delete roles", "delete.role"));
                await RoleManager.AddClaimAsync(adminrole, new Claim("Can edit roles", "edit.role"));
            }

            await UserManager.AddToRoleAsync(admin, "Admin");

            if (!roleCheck)
            {
                await UserManager.AddClaimAsync(admin, new Claim(ClaimTypes.Role, "Admin"));
            }
        }

        public static async Task CreateUsers(IServiceProvider serviceProvider, AppDbContext db)
        {
            if (db.Caregivers.Any() || db.Patients.Any())
            {
                return;
            }

            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<User>>();
            var listofusers = new List<User>()
                    {
                        new User {
                            UserName = "opiekun1Test",
                            Email = "opiekun1Test@mail.com",
                            PhoneNumber = "725252105",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "opiekun2Test",
                            Email = "opiekun2Test@mail.com",
                            PhoneNumber = "724857216",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "opiekun3Test",
                            Email = "opiekun3Test@mail.com",
                            PhoneNumber = "513736644",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent1Test",
                            Email = "pacjent1Test@mail.com",
                            PhoneNumber = "889083365",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent2Test",
                            Email = "pacjent2Test@mail.com",
                            PhoneNumber = "605418571",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent3Test",
                            Email = "pacjent3Test@mail.com",
                            PhoneNumber = "696918477",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent4Test",
                            Email = "pacjent4Test@mail.com",
                            PhoneNumber = "883795662",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent5Test",
                            Email = "pacjent5Test@mail.com",
                            PhoneNumber = "511156999",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent6Test",
                            Email = "pacjent6Test@mail.com",
                            PhoneNumber = "792525636",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent7Test",
                            Email = "pacjent7Test@mail.com",
                            PhoneNumber = "609152884",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent8Test",
                            Email = "pacjent8Test@mail.com",
                            PhoneNumber = "728732894",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent9Test",
                            Email = "pacjent9Test@mail.com",
                            PhoneNumber = "726063301",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent10Test",
                            Email = "pacjent10Test@mail.com",
                            PhoneNumber = "602981959",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent11Test",
                            Email = "pacjent1Test@mail.com",
                            PhoneNumber = "881425325",
                            AddDate = DateTime.Now },

                        new User {
                            UserName = "pacjent12Test",
                            Email = "pacjent2Test@mail.com",
                            PhoneNumber = "513415913",
                            AddDate = DateTime.Now },
                    };

            var i = 0;
            string [] firstnames = new [] {"Adrianna", "Jarosław", "Anna", "Marta", "Lucjan", "Edward", "Sylwia", "Gabriela", "Klaudia", "Daniel", "Julia", "Urszula", "Miłosz", "Danuta", "Zdzisława"};
            string [] lastnames = new [] {"Szymczak", "Sawicki", "Wiśniewska", "Wilk", "Janicki", "Stankiewicz", "Wierzbicka", "Nowakowska", "Polak", "Rutkowski", "Cieślak", "Sokołowska", "Szczepaniak", "Stępień", "Król"};
            string [] dates = new[] { "", "", "", "04.01.1932", "17.10.1933", "30.06.1934", "21.01.1950", "27.09.1944", "02.04.1942", "25.05.1949", "28.08.1940", "06.11.1947", "25.09.1952", "14.03.1951", "27.04.1937" };
            string [] comments = new[] {"Łagodne zaburzenia poznawcze.", "Demencja pierwsze stadium.", "Demencja drugie stadium.", "Choroba Alzheimera.", "Demencja naczyniowa." };
            foreach (var user in listofusers)
            {
                var checkUser = await UserManager.FindByNameAsync(user.UserName);
                if (checkUser == null)
                {
                    var result = await UserManager.CreateAsync(user, user.UserName);
                    if(result.Succeeded)
                    {
                        if (user.UserName.Contains("opiekun"))
                        {
                            await UserManager.AddToRoleAsync(user, "Caregiver");
                            await UserManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, "Caregiver"));
                            User copyuser = db.Users.FirstOrDefault(a => a.Id == user.Id);
                            Caregiver caregiver = new Caregiver { AddDate = user.AddDate, Email = user.Email, FirstName = firstnames[i], LastName = lastnames[i], PhoneNumber=user.PhoneNumber, User = copyuser };
                            db.Caregivers.Add(caregiver);
                            db.SaveChanges();
                            if (db.Patients.Any())
                            {
                                Caregiver copycaregiver = db.Caregivers.FirstOrDefault(a => a.Id == caregiver.Id);
                                foreach (var patient in db.Patients)
                                {
                                    db.CaregiverPatients.Add(new CaregiverPatient { Caregiver = copycaregiver, Patient = patient });
                                }
                                db.SaveChanges();
                            }
                        }
                        else if (user.UserName.Contains("pacjent"))
                        {
                            if(db.Caregivers.Any())
                            {
                                Random rand = new Random();
                                await UserManager.AddToRoleAsync(user, "Patient");
                                await UserManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, "Patient"));
                                User copyuser = db.Users.FirstOrDefault(a => a.Id == user.Id);
                                Patient patient = new Patient { AddDate = user.AddDate, Email = user.Email, FirstName = firstnames[i], LastName = lastnames[i], PhoneNumber = user.PhoneNumber, Date_of_birth = DateTime.Parse(dates[i]), Confirm = true, RoomNumber = rand.Next(200,400), Description = comments[rand.Next(0,5)], User = copyuser };
                                db.Patients.Add(patient);
                                db.SaveChanges();
                                if (db.Caregivers.Any())
                                {
                                    Patient copypatient = db.Patients.FirstOrDefault(a => a.Id == patient.Id);
                                    foreach (var caregiver in db.Caregivers)
                                    {
                                        db.CaregiverPatients.Add(new CaregiverPatient { Caregiver = caregiver, Patient = copypatient });
                                    }
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                IdentityResult unresult = await UserManager.DeleteAsync(user);
                            }
                        }
                        i++;
                    }
                }
            }
        }
    }
}
