﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace DementiaHelpApp.Models
{
    public class CaregiverPhoto
    {
        [Key]
        [Required]
        public string Id { get; set; }
        [Required]
        public string FileType { get; set; }
        public string FileName { get; set; }
        [Required]
        public string FilePath { get; set; }
        public byte[] FileBytes { get; set; }
        public int? FileLength { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime AddDate { get; set; }
        public DateTime UpdateDate { get; set; }

        [ForeignKey("Caregiver")]
        [Required]
        public string CaregiverId { get; set; }
        public virtual Caregiver Caregiver { get; set; }

        [ForeignKey("Reminder")]
        public string ReminderId { get; set; }
        public virtual Reminder Reminder { get; set; }
    }
}
