﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.Models
{
    public class Reminder
    {
        [Key]
        [Required]
        public string Id { get; set; }
        public string Name { get; set; }
        [Required]
        public DateTime AddDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Description { get; set; }

        [ForeignKey("Patient")]
        [Required]
        public string PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public virtual ICollection<PatientPhoto> PatientPhotos { get; set; }
        public virtual ICollection<CaregiverPhoto> CaregiverPhotos { get; set; }
    }
}
