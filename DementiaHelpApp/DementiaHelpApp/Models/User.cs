﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.Models
{
    public class User : IdentityUser
    {
        [Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime AddDate { get; internal set; }

        public virtual Caregiver Caregiver { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
