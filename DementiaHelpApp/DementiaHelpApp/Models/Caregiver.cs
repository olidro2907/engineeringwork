﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.Models
{
    public class Caregiver
    {
        [Key]
        [Required]
        public string Id { get; set; }
        
        [Required]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Numer telefonu")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Utworzony")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime AddDate { get; set; }

        [Display(Name = "Zedytowany")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime UpdateDate { get; set; }

        [ForeignKey("User")]
        [Required]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<CaregiverPhoto> CaregiverPhotos { get; set; }
        public virtual ICollection<CaregiverPatient> Patients { get; set; }
    }
}
