﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.Models
{
    public class CaregiverPatient
    {
        [ForeignKey("Caregiver")]
        [Required]
        public string CaregiverId { get; set; }
        public Caregiver Caregiver { get; set; }

        [ForeignKey("Patient")]
        [Required]
        public string PatientId { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
