﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DementiaHelpApp.Models
{
    public class Patient
    {
        [Key]
        [Required]
        public string Id { get; set; }

        public string Pesel { get; set; }

        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data urodzenia")]
        public DateTime Date_of_birth { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Numer telefonu")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Numer pokoju")]
        public int RoomNumber { get; set; }

        [Display(Name = "Utworzony")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime AddDate { get; set; }

        [Display(Name = "Zedytowany")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "Potwierdzony")]
        public bool Confirm { get; set; }

        [ForeignKey("User")]
        [Required]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<PatientPhoto> PatientPhotos { get; set; }
        public virtual ICollection<CaregiverPatient> Caregivers { get; set; }
    }
}
