﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DementiaHelpApp.Migrations
{
    public partial class ChangePhotoModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Photos_Caregivers_CaregiverId",
                table: "Photos");

            migrationBuilder.DropForeignKey(
                name: "FK_Reminders_Patients_PatientId",
                table: "Reminders");

            migrationBuilder.DropTable(
                name: "ReminderPhotos");

            migrationBuilder.DropIndex(
                name: "IX_Reminders_PatientId",
                table: "Reminders");

            migrationBuilder.DropIndex(
                name: "IX_Photos_CaregiverId",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "NowDate",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "PatientId",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "CaregiverId",
                table: "Photos");

            migrationBuilder.AlterColumn<string>(
                name: "FileType",
                table: "Photos",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Photos",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FileLength",
                table: "Photos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "Photos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReminderId",
                table: "Photos",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Photos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Photos_ReminderId",
                table: "Photos",
                column: "ReminderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Photos_Reminders_ReminderId",
                table: "Photos",
                column: "ReminderId",
                principalTable: "Reminders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Photos_Reminders_ReminderId",
                table: "Photos");

            migrationBuilder.DropIndex(
                name: "IX_Photos_ReminderId",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "FileLength",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Photos");

            migrationBuilder.AddColumn<DateTime>(
                name: "NowDate",
                table: "Reminders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "PatientId",
                table: "Reminders",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "FileType",
                table: "Photos",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Photos",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "CaregiverId",
                table: "Photos",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "ReminderPhotos",
                columns: table => new
                {
                    ReminderId = table.Column<string>(nullable: false),
                    PhotoId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReminderPhotos", x => new { x.ReminderId, x.PhotoId });
                    table.ForeignKey(
                        name: "FK_ReminderPhotos_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReminderPhotos_Reminders_ReminderId",
                        column: x => x.ReminderId,
                        principalTable: "Reminders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reminders_PatientId",
                table: "Reminders",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_CaregiverId",
                table: "Photos",
                column: "CaregiverId");

            migrationBuilder.CreateIndex(
                name: "IX_ReminderPhotos_PhotoId",
                table: "ReminderPhotos",
                column: "PhotoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Photos_Caregivers_CaregiverId",
                table: "Photos",
                column: "CaregiverId",
                principalTable: "Caregivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reminders_Patients_PatientId",
                table: "Reminders",
                column: "PatientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
