﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DementiaHelpApp.Migrations
{
    public partial class ChangeRaltionalPatientAndCaregiver : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Caregivers_CaregiverId",
                table: "Patients");

            migrationBuilder.DropIndex(
                name: "IX_Patients_CaregiverId",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "CaregiverId",
                table: "Patients");

            migrationBuilder.CreateTable(
                name: "CaregiverPatient",
                columns: table => new
                {
                    CaregiverId = table.Column<string>(nullable: false),
                    PatientId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaregiverPatient", x => new { x.CaregiverId, x.PatientId });
                    table.ForeignKey(
                        name: "FK_CaregiverPatient_Caregivers_CaregiverId",
                        column: x => x.CaregiverId,
                        principalTable: "Caregivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CaregiverPatient_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CaregiverPatient_PatientId",
                table: "CaregiverPatient",
                column: "PatientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CaregiverPatient");

            migrationBuilder.AddColumn<string>(
                name: "CaregiverId",
                table: "Patients",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_CaregiverId",
                table: "Patients",
                column: "CaregiverId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Caregivers_CaregiverId",
                table: "Patients",
                column: "CaregiverId",
                principalTable: "Caregivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
