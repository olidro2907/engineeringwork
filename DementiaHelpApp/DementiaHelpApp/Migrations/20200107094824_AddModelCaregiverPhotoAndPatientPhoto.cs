﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DementiaHelpApp.Migrations
{
    public partial class AddModelCaregiverPhotoAndPatientPhoto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Photos");

            migrationBuilder.AddColumn<string>(
                name: "PatientId",
                table: "Reminders",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Reminders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "CaregiverPhotos",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    FileType = table.Column<string>(nullable: false),
                    FileName = table.Column<string>(nullable: true),
                    FilePath = table.Column<string>(nullable: false),
                    FileBytes = table.Column<byte[]>(nullable: true),
                    FileLength = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: false),
                    AddDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    CaregiverId = table.Column<string>(nullable: false),
                    ReminderId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaregiverPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaregiverPhotos_Caregivers_CaregiverId",
                        column: x => x.CaregiverId,
                        principalTable: "Caregivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CaregiverPhotos_Reminders_ReminderId",
                        column: x => x.ReminderId,
                        principalTable: "Reminders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PatientPhotos",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    FileType = table.Column<string>(nullable: false),
                    FileName = table.Column<string>(nullable: true),
                    FilePath = table.Column<string>(nullable: false),
                    FileBytes = table.Column<byte[]>(nullable: true),
                    FileLength = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: false),
                    AddDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    PatientId = table.Column<string>(nullable: false),
                    ReminderId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PatientPhotos_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PatientPhotos_Reminders_ReminderId",
                        column: x => x.ReminderId,
                        principalTable: "Reminders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reminders_PatientId",
                table: "Reminders",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_CaregiverPhotos_CaregiverId",
                table: "CaregiverPhotos",
                column: "CaregiverId");

            migrationBuilder.CreateIndex(
                name: "IX_CaregiverPhotos_ReminderId",
                table: "CaregiverPhotos",
                column: "ReminderId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientPhotos_PatientId",
                table: "PatientPhotos",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientPhotos_ReminderId",
                table: "PatientPhotos",
                column: "ReminderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reminders_Patients_PatientId",
                table: "Reminders",
                column: "PatientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reminders_Patients_PatientId",
                table: "Reminders");

            migrationBuilder.DropTable(
                name: "CaregiverPhotos");

            migrationBuilder.DropTable(
                name: "PatientPhotos");

            migrationBuilder.DropIndex(
                name: "IX_Reminders_PatientId",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "PatientId",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Reminders");

            migrationBuilder.CreateTable(
                name: "Photos",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AddDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    FileBytes = table.Column<byte[]>(nullable: true),
                    FileLength = table.Column<int>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    FilePath = table.Column<string>(nullable: false),
                    FileType = table.Column<string>(nullable: false),
                    PatientId = table.Column<string>(nullable: false),
                    ReminderId = table.Column<string>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Photos_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Photos_Reminders_ReminderId",
                        column: x => x.ReminderId,
                        principalTable: "Reminders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Photos_PatientId",
                table: "Photos",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_ReminderId",
                table: "Photos",
                column: "ReminderId");
        }
    }
}
