﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DementiaHelpApp.Migrations
{
    public partial class PhotoToCargiverAndPatient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Reminders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Reminders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CaregiverId",
                table: "Photos",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PatientId",
                table: "Photos",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Patients",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Caregivers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Photos_CaregiverId",
                table: "Photos",
                column: "CaregiverId");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_PatientId",
                table: "Photos",
                column: "PatientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Photos_Caregivers_CaregiverId",
                table: "Photos",
                column: "CaregiverId",
                principalTable: "Caregivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Photos_Patients_PatientId",
                table: "Photos",
                column: "PatientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Photos_Caregivers_CaregiverId",
                table: "Photos");

            migrationBuilder.DropForeignKey(
                name: "FK_Photos_Patients_PatientId",
                table: "Photos");

            migrationBuilder.DropIndex(
                name: "IX_Photos_CaregiverId",
                table: "Photos");

            migrationBuilder.DropIndex(
                name: "IX_Photos_PatientId",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "CaregiverId",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "PatientId",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Caregivers");
        }
    }
}
