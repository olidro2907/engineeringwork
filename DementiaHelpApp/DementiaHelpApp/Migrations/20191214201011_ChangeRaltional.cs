﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DementiaHelpApp.Migrations
{
    public partial class ChangeRaltional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaregiverPatient_Caregivers_CaregiverId",
                table: "CaregiverPatient");

            migrationBuilder.DropForeignKey(
                name: "FK_CaregiverPatient_Patients_PatientId",
                table: "CaregiverPatient");

            migrationBuilder.DropForeignKey(
                name: "FK_ReminderPhoto_Photos_PhotoId",
                table: "ReminderPhoto");

            migrationBuilder.DropForeignKey(
                name: "FK_ReminderPhoto_Reminders_ReminderId",
                table: "ReminderPhoto");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ReminderPhoto",
                table: "ReminderPhoto");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CaregiverPatient",
                table: "CaregiverPatient");

            migrationBuilder.RenameTable(
                name: "ReminderPhoto",
                newName: "ReminderPhotos");

            migrationBuilder.RenameTable(
                name: "CaregiverPatient",
                newName: "CaregiverPatients");

            migrationBuilder.RenameIndex(
                name: "IX_ReminderPhoto_PhotoId",
                table: "ReminderPhotos",
                newName: "IX_ReminderPhotos_PhotoId");

            migrationBuilder.RenameIndex(
                name: "IX_CaregiverPatient_PatientId",
                table: "CaregiverPatients",
                newName: "IX_CaregiverPatients_PatientId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReminderPhotos",
                table: "ReminderPhotos",
                columns: new[] { "ReminderId", "PhotoId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_CaregiverPatients",
                table: "CaregiverPatients",
                columns: new[] { "CaregiverId", "PatientId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CaregiverPatients_Caregivers_CaregiverId",
                table: "CaregiverPatients",
                column: "CaregiverId",
                principalTable: "Caregivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CaregiverPatients_Patients_PatientId",
                table: "CaregiverPatients",
                column: "PatientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ReminderPhotos_Photos_PhotoId",
                table: "ReminderPhotos",
                column: "PhotoId",
                principalTable: "Photos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ReminderPhotos_Reminders_ReminderId",
                table: "ReminderPhotos",
                column: "ReminderId",
                principalTable: "Reminders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaregiverPatients_Caregivers_CaregiverId",
                table: "CaregiverPatients");

            migrationBuilder.DropForeignKey(
                name: "FK_CaregiverPatients_Patients_PatientId",
                table: "CaregiverPatients");

            migrationBuilder.DropForeignKey(
                name: "FK_ReminderPhotos_Photos_PhotoId",
                table: "ReminderPhotos");

            migrationBuilder.DropForeignKey(
                name: "FK_ReminderPhotos_Reminders_ReminderId",
                table: "ReminderPhotos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ReminderPhotos",
                table: "ReminderPhotos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CaregiverPatients",
                table: "CaregiverPatients");

            migrationBuilder.RenameTable(
                name: "ReminderPhotos",
                newName: "ReminderPhoto");

            migrationBuilder.RenameTable(
                name: "CaregiverPatients",
                newName: "CaregiverPatient");

            migrationBuilder.RenameIndex(
                name: "IX_ReminderPhotos_PhotoId",
                table: "ReminderPhoto",
                newName: "IX_ReminderPhoto_PhotoId");

            migrationBuilder.RenameIndex(
                name: "IX_CaregiverPatients_PatientId",
                table: "CaregiverPatient",
                newName: "IX_CaregiverPatient_PatientId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReminderPhoto",
                table: "ReminderPhoto",
                columns: new[] { "ReminderId", "PhotoId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_CaregiverPatient",
                table: "CaregiverPatient",
                columns: new[] { "CaregiverId", "PatientId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CaregiverPatient_Caregivers_CaregiverId",
                table: "CaregiverPatient",
                column: "CaregiverId",
                principalTable: "Caregivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CaregiverPatient_Patients_PatientId",
                table: "CaregiverPatient",
                column: "PatientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ReminderPhoto_Photos_PhotoId",
                table: "ReminderPhoto",
                column: "PhotoId",
                principalTable: "Photos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ReminderPhoto_Reminders_ReminderId",
                table: "ReminderPhoto",
                column: "ReminderId",
                principalTable: "Reminders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
