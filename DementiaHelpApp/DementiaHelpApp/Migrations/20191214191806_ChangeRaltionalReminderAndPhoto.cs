﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DementiaHelpApp.Migrations
{
    public partial class ChangeRaltionalReminderAndPhoto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Photos_Reminders_ReminderId",
                table: "Photos");

            migrationBuilder.DropIndex(
                name: "IX_Photos_ReminderId",
                table: "Photos");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "Photos");

            migrationBuilder.CreateTable(
                name: "ReminderPhoto",
                columns: table => new
                {
                    ReminderId = table.Column<string>(nullable: false),
                    PhotoId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReminderPhoto", x => new { x.ReminderId, x.PhotoId });
                    table.ForeignKey(
                        name: "FK_ReminderPhoto_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReminderPhoto_Reminders_ReminderId",
                        column: x => x.ReminderId,
                        principalTable: "Reminders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ReminderPhoto_PhotoId",
                table: "ReminderPhoto",
                column: "PhotoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReminderPhoto");

            migrationBuilder.AddColumn<string>(
                name: "ReminderId",
                table: "Photos",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_ReminderId",
                table: "Photos",
                column: "ReminderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Photos_Reminders_ReminderId",
                table: "Photos",
                column: "ReminderId",
                principalTable: "Reminders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
